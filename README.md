# Kakeya

A program that can be used to create Kakeya sets of arbitrary dimension over arbitrary finite fields. It tries to generate Kakeya sets which are as small as possible.
